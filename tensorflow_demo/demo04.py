import tensorflow as tf  # 导入 TF 库
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

x = tf.linspace(-8., 8, 100)  # 设置 x 坐标的间隔
print(x)
y = tf.linspace(-8., 8, 100)  # 设置 y 坐标的间隔
x, y = tf.meshgrid(x, y)  # 生成网格点，并拆分后返回
print(x.shape, y.shape)  # 打印拆分后的所有点的 x,y 坐标张量 shape
fig = plt.figure()
ax = Axes3D(fig)
# z = tf.sqrt(x ** 2 + y ** 2)
z = x ** 2 + y ** 2
z = tf.sin(z) / z  # sinc 函数实现

# 根据网格点绘制 sinc 函数 3D 曲面
ax.contour3D(x.numpy(), y.numpy(), z.numpy(), 50)
plt.show()
