import tensorflow as tf
from tensorflow import keras

layers = tf.keras.layers
Sequential = tf.keras.Sequential
# 优化器
optimizers = tf.keras.optimizers
# 损失函数
losses = tf.keras.losses

# 加载 ImageNet 预训练网络模型，并去掉最后一层
resnet = keras.applications.ResNet50(weights='imagenet', include_top=False)
resnet.summary()
# 测试网络的输出
x = tf.random.normal([4, 224, 224, 3])
out = resnet(x)
# 测试版(20191108)
# 8.5 模型乐园  [在此处键入]  11
print(out.shape)
