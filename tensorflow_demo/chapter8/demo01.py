import tensorflow as tf
from tensorflow import keras

layers = tf.keras.layers
Sequential = tf.keras.Sequential
# 优化器
optimizers = tf.keras.optimizers
# 损失函数
losses = tf.keras.losses

network = Sequential([layers.Dense(256, activation='relu'),
                      layers.Dense(128, activation='relu'),
                      layers.Dense(64, activation='relu'),
                      layers.Dense(32, activation='relu'),
                      layers.Dense(10)])
network.build(input_shape=(None, 28 * 28))
network.summary()  # 打印网络结构和参数量

network.compile(optimizer=optimizers.Adam(lr=0.01),
                loss=losses.CategoricalCrossentropy(from_logits=True),
                metrics=['accuracy']  # 设置测量指标为准确率
                )
# 指定训练集为 train_db，验证集为 val_db,训练 5 个 epochs，每 2 个 epoch 验证一次
# 返回训练信息保存在 history 中
# history = network.fit(train_db, epochs=5, validation_data=val_db,
#                       validation_freq=2)
