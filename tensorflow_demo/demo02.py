import numpy as np
import tensorflow as tf  # 导入 TF 库
from tensorflow_core.python import layers

out = tf.random.normal([4, 10])
# print(out)
y = tf.constant([1, 2, 2, 0])
print(y)
y = tf.one_hot(y, depth=10)
# 计算每个样本的误差
loss = tf.keras.losses.mse(y, out)
print(loss)
loss = tf.reduce_mean(loss)
print(loss)

print("==================")
a = tf.random.normal([2, 10])
a = tf.nn.softmax(a, axis=1)
print(a)
pred = tf.argmax(a, axis=1)
print(pred)
