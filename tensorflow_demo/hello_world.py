import tensorflow as tf
import os

layers = tf.keras.layers
version = tf.__version__

gpu_ok = tf.test.is_gpu_available()
print("tf version:", version, "\nuse GPU", gpu_ok)
