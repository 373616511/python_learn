import tensorflow as tf

layers = tf.keras.layers

cell = layers.SimpleRNNCell(3)
cell.build(input_shape=(None, 4))
print(cell.trainable_variables)
