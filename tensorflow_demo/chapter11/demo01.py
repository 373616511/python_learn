import tensorflow as tf

layers = tf.keras.layers

x = tf.range(10)
print(x)
x = tf.random.shuffle(x)
print(x)
net = layers.Embedding(10, 4)
out = net(x)
print(out)
print('-----')
print(net(0))
# print(net.embeddings.trainable)

# 从预训练模型中加载词向量表
# embed_glove = load_embed('glove.6B.50d.txt')
# 直接利用预训练的词向量表初始化 Embedding 层
# net.set_weights([embed_glove])
