import tensorflow as tf

x = tf.random.normal([2, 5, 5, 3])
# print(x)
w = tf.random.normal([3, 3, 3, 4])
out = tf.nn.conv2d(x, w, strides=1, padding=[[0, 0], [1, 1], [1, 1], [0, 0]])

print(out)
