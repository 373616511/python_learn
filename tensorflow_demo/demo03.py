import numpy as np
import tensorflow as tf  # 导入 TF 库
from tensorflow_core.python import layers

out = tf.random.normal([100, 10])
out = tf.nn.softmax(out, axis=1)
pred = tf.argmax(out, axis=1)
print(pred)

y = tf.random.uniform([100], dtype=tf.int64, maxval=10)
print(y)

out = tf.equal(pred, y)
print(out)

out = tf.cast(out, dtype=tf.float32)
correct = tf.reduce_sum(out, axis=0)

print(correct)
