import re

if __name__ == '__main__':
    print("start")
    # into(\s*)abc

    match = re.match(r"/(.*)/(.*)/(.*)/", "/usr/local/bin/")
    print(match.groups())

    match = re.match(r'INTO(\n\s*\r*)' + 'ABC', "into   abcd ".upper())
    if match:
        print(match.groups())
    else:
        print('not match!')
