#!/usr/bin/python
import sys
import datetime
import time
import pyotp

if __name__ == '__main__':
    sec = 11384456629885107018982981141862688548123215886135943315855304062317858839454535579103373934439929453413083358224853097381649117302437604377782324684312676
    # sec = 'base32secret3232'
    print(type(sec))
    sec = pyotp.random_base32(40, sec.to_bytes(1, byteorder='big'))

    print(sec)
    totp = pyotp.TOTP(s=sec)
    pwd = totp.now()
    print(pwd)

    # OTP verified for current time
    a = totp.verify(pwd)  # => True
    print(a)
    # 30秒后会失效
    # time.sleep(30)
    b = totp.verify(pwd)  # => False
    print(b)
