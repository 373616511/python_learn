import scrapy
#虽然报错，但是能在命令行执行
from properties.items import PropertiesItem



class BasicSpider(scrapy.Spider):
    name = 'basic'
    allowed_domains = ['web']
    start_urls = ['https://www.baidu.com']

    def parse(self, response):
        print('parse函数执行')
        xpath = '//*[@id="lg"]'
        a = response.xpath(xpath).extract()
        # print(a)
        item = PropertiesItem()
        item['title'] = response.xpath('//*[@id="lg"]').extract()
        return item
