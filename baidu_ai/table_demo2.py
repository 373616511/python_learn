import quick_start as qs
import base64
from urllib.parse import quote
import requests

headers = {
    'Content-Type': "application/x-www-form-urlencoded",
    'charset': "utf-8"
}
if __name__ == '__main__':
    recognise_api_url = "https://aip.baidubce.com/rest/2.0/solution/v1/iocr/recognise"
    access_token = "your_access_token"
    access_token = qs.fetch_token()
    templateSign = "d37701a933f40b48d071641a1fa142e7"
    # 测试数据路径
    image_path = "./shouxie.jpg"
    try:
        with open(image_path, 'rb') as f:
            image_data = f.read()
        image_b64 = base64.b64encode(image_data).decode().replace("\r", "")
        # 请求模板的bodys
        recognise_bodys = "access_token=" + access_token + "&templateSign=" + templateSign + \
                          "&image=" + quote(image_b64.encode("utf8"))
        # 请求模板识别
        response = requests.post(recognise_api_url, data=recognise_bodys, headers=headers)

        print(response.text)
    except Exception as e:
        print(e)
