# 浮点类型精度
from decimal import *

x = 99.95
print(x + 0.01)

a = Decimal(3.03)
print(a)
b = Decimal('3.03')
print(b)

a = Decimal('99.95')
b = Decimal('0.01')
print(a)
print(b)
print(a + b)
print(a + 1)
