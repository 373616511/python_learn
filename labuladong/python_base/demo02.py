# 将ASCII字符转换为对应的数值即‘a’-->65，使用ord函数,ord('a')
# 反正，使用chr函数，将数值转换为对应的ASCII字符，chr(65)

# 用户输入字符
c = input("请输入一个字符: ")

# 用户输入ASCII码，并将输入的数字转为整型
a = int(input("请输入一个ASCII码: "))

print(c + " 的ASCII 码为", ord(c))
print(a, " 对应的字符为", chr(a))
