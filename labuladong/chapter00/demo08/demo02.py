# 优化后的函数-优化所有的if
# 跳过重复元素
a = [1, 3, 1, 2, 2, 3]
a.sort()


def two_sum(nums: list, target):
    lo = 0
    hi = len(nums) - 1
    rs = []
    while lo < hi:
        s = nums[lo] + nums[hi]
        left = nums[lo]
        right = nums[hi]
        if s == target:
            rs.append([nums[lo], nums[hi]])
            while lo < hi and nums[lo] == left:
                lo += 1
            while lo < hi and nums[hi] == right:
                hi -= 1
        elif s > target:  # 最大值加上左边的都大于target所以左移右指针
            while lo < hi and nums[hi] == right:
                hi -= 1
        elif s < target:
            while lo < hi and nums[lo] == left:
                lo += 1
    return rs


print(two_sum(a, 4))
