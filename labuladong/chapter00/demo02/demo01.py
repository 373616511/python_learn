# 全排列问题
res = []


# 我们这⾥稍微做了些变通，没有显式记录「选择列表」，⽽是通过  nums和  track  推导出当前的选择列表
def back_track(nums: list[int], track: list[int]):
    # 触发结束条件
    if len(track) == len(nums):
        res.append(track[:])  # 深拷贝
    for i, v in enumerate(nums):
        # 排除不合法的选择
        if v in track:
            continue
        # 做选择
        track.append(v)
        # 进入下一层决策树
        back_track(nums, track)
        # 取消选择
        track.remove(v)


if __name__ == '__main__':
    a = [1, 2, 3]
    t = []
    back_track(a, t)
    print(res)
