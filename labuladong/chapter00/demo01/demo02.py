# 凑零钱问题
def coin_change(coins: list[int], amount: int):
    def dp(n):
        # base case
        if n == 0:
            return 0
        if n < 0:
            return -1
        # 求最⼩值，所以初始化为正⽆穷
        res = float('INF')
        for coin in coins:
            sub_problem = dp(n - coin)
            # ⼦问题⽆解，跳过
            if sub_problem == -1:
                continue
            res = min(res, 1 + sub_problem)

        return res if res != float('INF') else -1

    return dp(amount)


a = [1, 2, 5]
print(coin_change(a, 11))
