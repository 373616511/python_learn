cars = ['audi', 'bmw', 'subaru', 'toyota']
for car in cars:
    if car == 'bmw':
        print(car.upper())
    else:
        print(car.title())

# print(1 == 1)
# print(1 == '1')
# print(1 != 'a')

# print(1 == 1 and 1 == 2)
# print(1 == 1 or 1 <= 2 or True)

words = ['a', 'b', 'c', 'd', 'e', 'f']
print('a' in words)
print('as' not in words)

age = 5
if age < 6:
    print("666")
elif age < 16:
    print("1616")
else:
    print("else")
