"""learning_log URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url

urlpatterns = [
    path('admin/', admin.site.urls),
    #url(r'', include('learning_logs.urls', namespace='learning_logs')),
    #Django2.0写法
    url(r'', include(('learning_logs.urls', 'learning_logs'), namespace='learning_logs')),
    #我们添加了一行代码，以包含应用程序 users 中的文件 urls.py 。这行代码与任何以单
    #词 users 打头的 URL （如 http://localhost:8000/users/login/ ）都匹配。我
    #们还创建了命名空间 'users' ，以便将应用程序 learning_logs 的 URL 同应用程序 
    #users 的 URL 区分开来。
    url(r'^users/', include(('users.urls','users'), namespace='users')),
]
