from django import forms
from .models import Topic, Entry

class TopicForm(forms.ModelForm):
	class Meta:
		model = Topic
		fields = ['text']
		labels = {'text': ''}
		
class EntryForm(forms.ModelForm):
	class Meta:
		model = Entry
		fields = ['text']
		labels = {'text': ''}
		#定义了属性 widgets 。 小部件 （ widget ）是一个 HTML 表单元素，
		#如单行文本框、多行文本区域或下拉列表。通过设置属性 widgets ，可覆
		#盖 Django 选择的默认小部件。
		widgsts = {'text': forms.Textarea(attrs={'cols': 80})}
