filename = 'programming.txt'

# with open(filename, 'w') as f:
# 附加模式（第二个参数） 给文件添加内容，而不是覆盖原有的内容
with open(filename, 'a') as f:
    # print(open.__doc__)
    f.write('a\n')
    f.write('b\n')
