# 异常捕获
try:
    print(5 / 0)
# print(5 / 3)
except Exception as e:
    # pass 让python什么都不要做 不再打印错误信息
    # pass
    print('错误信息:' + str(e))
else:
    # 依赖于 try 代码块成功执行的代码都应放到 else 代码块中
    print('else execute!')
