import pygal_maps_world.maps as pygal

wm = pygal.World()
wm.title = '地图'

wm.add('North America', {'ca': 34126000, 'us': 309349000, 'mx': 113423000})

wm.render_to_file('na_populations.svg')
