from pygal_maps_world.i18n import COUNTRIES

def get_country_code(country_name):
	"""根据指定的国家，返回Pygal使用的两个字母的国别编码"""
	
	for code,name in COUNTRIES.items():
		if name == country_name:
			return code
	
	#如果没有找到国家返回None
	return None

print(get_country_code('Andorra'))
print(get_country_code('china'))

