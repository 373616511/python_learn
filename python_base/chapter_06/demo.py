#字典与JSON
# 从本质上讲，字典是一种数据结构，而json是一种格式；字典有很多内置函数，有多种调用方法，而json是数据打包的一种格式，
# 并不像字典具备操作性，并且是格式就会有一些形式上的限制，比如json的格式要求必须且只能使用双引号作为key或者值的边界
# 符号，不能使用单引号，而且“key”必须使用边界符（双引号），但字典就无所谓了。

alien_0 = {'color': 'green', 'points': 5}
print(alien_0['color'])
print(alien_0['points'])

alien_0['x_position'] = 0
alien_0['y_position'] = 25
print(alien_0)

# del alien_0['points']
# print(alien_0)
print("遍历字典")
# for key, value in alien_0.items():
#     print(key + ' - ' + str(value))

# for n in alien_0.keys():
#     print(n)

print('嵌套')
alien_0 = {'color': 'green', 'points': 5}
alien_1 = {'color': 'yellow', 'points': 10}
alien_2 = {'color': 'red', 'points': 15}
aliens = [alien_0, alien_1, alien_2]
print(aliens)
aliens[2]['color']= 'black'
print(aliens)
