import matplotlib.pyplot as plt

#x_values = [1,2,3,4,5]
#y_values = [1,4,9,16,25]
x_values = list(range(1,1001))
y_values = [x**2 for x in x_values]

#设置Figure大小,(12,8) -> 1200 800像素
plt.figure(figsize=(12,8))

#s 代表点的尺寸
#plt.scatter(2, 4, s = 200)
plt.scatter(x_values, y_values, c=y_values, cmap=plt.cm.Blues, edgecolor='none', s = 40)

#设置图表标题并给坐标抽加上标签
plt.title("Squares Numbers", fontsize = 24)
plt.xlabel("Value",fontsize = 14)
plt.ylabel("Value of Value", fontsize = 14)

#设置刻度标记的大小
plt.tick_params(axis='both',which='major', labelsize = 14)

#plt.show()

plt.savefig('square_plot.png', bbox='tight')
#plt.savefig('square_plot.png')
