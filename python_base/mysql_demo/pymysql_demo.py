import pymysql as ps

db = ps.connect('localhost', 'root', 'root', 'mydb')
print(db)
# 使用 cursor() 方法创建一个游标对象 cursor
cursor = db.cursor()
cursor.execute("select version()")
data = cursor.fetchone()
print("版本:", data)

# 插入数据
sql = "insert into demo values (%s, %s)"
val = ('3', '王五')
# try:
#     cursor.execute(sql, val)
#     db.commit()
# except Exception as e:
#     print(e)
#     db.rollback()

cursor.close()

db.close()
