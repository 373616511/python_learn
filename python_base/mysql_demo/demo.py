import mysql.connector as mc

# pip install mysql-connector-python
# pip install mysql-connector
# 都安装才能使用

mydb = mc.connect(
    host="localhost",  # 数据库主机地址
    user="root",  # 数据库用户名
    passwd="root",  # 数据库密码
    database='mydb'
)
print(mydb)
mycursor = mydb.cursor()

sql = "update demo set name = %s where name = %s"
na = ('张三', '李四')
mycursor.execute(sql, na)

mydb.commit()

