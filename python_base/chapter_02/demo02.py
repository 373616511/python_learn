# Python基本数据类型一般分为：数字、字符串、列表、元组、字典、集合这六种基本数据类型。
#https://blog.csdn.net/qq_41913882/article/details/82183700

# bool，从Python2.3开始Python中添加了布尔类型。布尔类型有两种True和False。对于没有__nozero__方法的对象默认是True。

# 对于值为0的数字、空集（空列表、空元组、空字典等）在Python中的布尔类型中都是False。

print(bool(''))
print(bool(0))
print(bool(()))
print(bool(-1))
