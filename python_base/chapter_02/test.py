message = 'hello python world'
print(message.title())
print(message.upper())
print(message.lower())

message = ' python '
print(message)
print(message.strip())
print(message.rstrip())
print(message.lstrip())

print(0.2 + 0.1)
age = 23
print("Happy " + str(age) + "rd Birthday")

#注释
import this