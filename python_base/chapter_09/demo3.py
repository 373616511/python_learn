from python_base.chapter_09.my_car import Car

c = Car('大众汽车', '桑塔纳', 2000)
descriptive_name = c.get_descriptive_name()
print(descriptive_name)

# 文档字符串
print(Car.__doc__)
print(c.__doc__)
