"""python 标准库"""
from collections import OrderedDict

# 创建字典并记录其中的键 — 值对的添加顺序
a = OrderedDict()
a['a'] = 1878
a['b'] = 3
a['c'] = 56
print(a)
print(a.__doc__)
