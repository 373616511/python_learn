class Car():
    """汽车类"""

    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year
        self.odometer_reading = 0

    def get_descriptive_name(self):
        long_name = str(self.year) + ' ' + self.make + ' ' + self.model
        return long_name.title()

    def read_odometer(self):
        print("This car has " + str(self.odometer_reading) + " miles on it.")

    def update_odometer(self, mileage):
        if (mileage > self.odometer_reading):
            self.odometer_reading = mileage
        else:
            print('不能回调里程表')

    def increment_odometer(self, miles):
        self.odometer_reading += miles

    def fill_gas_tank(self):
        print('加油')


class ElectricCar(Car):
    """电动汽车的独特之处"""

    def __init__(self, make, model, year):
        """初始化父类的属性"""
        super().__init__(make, model, year)
        # self.battery_size = 90
        self.battery = Battery(100)

    def fill_gas_tank(self):
        """重写fill_gas_tank方法"""
        print('特斯拉没有油箱')


# 将实例作为属性
class Battery():
    def __init__(self, battery_size=70):
        self.battery_size = battery_size

    def describe_battery(self):
        """ 打印一条描述电瓶容量的消息 """
        print("This car has a " + str(self.battery_size) + "-kWh battery.")
