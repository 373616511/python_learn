# 切片
players = ['charles', 'martina', 'michael', 'florence', 'eli']
print(players[0:3])

# 复制
my_foods = ['pizza', 'falafel', 'carrot cake']
# 这行不通（会包含全部的food）
# friend_foods = my_foods
friend_foods = my_foods[:]
my_foods.append('cannoli')
friend_foods.append('ice cream')
print("My favorite foods are:")
print(my_foods)
print("\nMy friend's favorite foods are:")
print(friend_foods)
