for value in range(1,5):
    print(value)

numbers = list(range(1,11))
print(numbers)


squares = []
for value in range(1,10):
    square = value**2
    squares.append(square)
print(squares)

print(max(squares))
print(min(squares))
print(sum(squares))

squares = [value**3 for value in range(1,6)]
print(squares)

b = squares[:]
b.append('23')
print(b)
print(squares)
