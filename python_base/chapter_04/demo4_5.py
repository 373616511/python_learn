# 元组 Python 将不能修改的值称为 不可变的 ，而不可变的列表被称为 元组
dimensions = (200, 50)
print(dimensions[0])
print(dimensions[1])
# 元组是不可以修改的，执行下面语句会报错：TypeError: 'tuple' object does not support item assignment
# dimensions[0] = 210

for d in dimensions:
    print(d)

# 虽然不能修改元组的元素，但可以给存储元组的变量赋值
dimensions = (200, 50)
print("Original dimensions:")
for dimension in dimensions:
    print(dimension)
dimensions = (400, 100)
print("\nModified dimensions:")
for dimension in dimensions:
    print(dimension)
