for i in range(1, 5):
    print(i)

# 使用函数 list() 将 range() 的结果直接转换为列表
members = list(range(1, 6))
print(members)

# 从 2 开始数，然后不断地加 2 ，直到达到或超过终值（ 11 ）
even_numbers = list(range(2, 11, 2))
print(even_numbers)

# 对数字列表执行简单的统计计算
print(min(members))
print(max(members))
print(sum(members))

# 列表解析
squares = [value ** 2 for value in range(1, 11)]
print(squares)
