bicycles = ['trek', 'cannondale', 'redline', 'specialized']
print(bicycles)
print(bicycles[0])
#获取列表汇总最后一个元素
print(bicycles[-1])

bicycles.append('add')
print(bicycles)

bicycles.insert(1,'insert')
print(bicycles)

del bicycles[1]
print(bicycles)

pop = bicycles.pop(1)
print(bicycles)
print(pop)

bicycles.remove('add')
print(bicycles)

bicycles = ['trek', 'cannondale', 'redline', 'specialized']
# bicycles.sort(reverse=True)
# print(bicycles)
print(sorted(bicycles))
print(bicycles)

print(bicycles.reverse())

print(len(bicycles))