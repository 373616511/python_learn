tup = ('a', 2, 'ee')
print(type(tup))

# 不加逗号，int
tup = (50)
print(type(tup))
# 加逗号，tuple
tup = (50,)
print(type(tup))

# 访问元祖
tup = ('a', 2, 'ee')
print(tup[1:3])

# 元祖不能修改
# tup[1] = 3

tup2 = (50,)
tup3 = tup + tup2
print(tup3)

# 元组中的元素值是不允许删除的，但我们可以使用del语句来删除整个元组
# del tup3
# print(tup3)

# 运算符
# 计算元素格式
print(len(tup3))
# 连接
print((1,) + (2, 3))
# 复制
print(tup3 * 4)
# 元素是否存在
print(2 in tup3)
# 迭代
for x in tup3:
    print(x)

# 截取 索引
print('索引截取')
l = ('a', 'b', 'c')
print(l[1])
# 反向读取；读取倒数第二个元素
print(l[-2])
# 截取元素，从第二个开始后的所有元素。
print(l[1:])

print('元祖内置函数')
tuple2 = ('5', '4', '8')
print(max(tuple2))
print(min(tuple2))
list = ['a', 'b']
print('将列表转换成元祖', tuple(list))

print('元祖不可变')
# 内存地址不一样了
t = (1, 23, 33)
print(id(t))
t = (2,)
print(id(t))
