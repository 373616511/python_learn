# 使用默认值时，在形参列表中必须先列出没有默认值的形参，再列出有默认值的实参。这让 Python 依然能够正确地解读位置实参。
def greet_user(username, a='bbb'):
    """显示简单的问候语"""
    print("hello " + username + ' ' + a)

    # 关键字实参
    # greet_user(username='lisi', a='a')


greet_user('zhangsan', 'aa')


def build_person(first_name, last_name):
    """ 返回一个字典，其中包含有关一个人的信息 """
    person = {'first': first_name, 'last': last_name}
    return person


musician = build_person('jimi', 'hendrix')
print(musician)

a = [1, 2, 3, 4, 5]


def print_a(a):
    while a:
        a1 = a.pop()
        print(a1)


print_a(a[:])
print(a)


def make_pizza(size, *toppings):
    """打印顾客点的所有配料"""
    print(toppings)


make_pizza(2, 'a')
make_pizza(16, '1', 'b')
