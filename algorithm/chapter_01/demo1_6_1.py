tab = [1, 2, 3, 56, 788]
print(tab)

# for 前面带了一个参数
m = max((tab[i], i) for i in range(len(tab)))
print(m)

# 相当于下面的语句
tuples = []
for i in range(len(tab)):
    tuples.append((tab[i], i))
print(tuples)
print(max(tuples))

# tab2 = [str(tab[i]) for i in range(len(tab))]

# print(tab2)

# 相当于下面的语句
tab3 = []
for i in range(len(tab)):
    tab3.append(str(tab[i]))
print(tab3)


# 用字典来统计每个元素的出现次数，并用以上代码来选择其中的多数元素
def majority(words):
    compute = {}
    for word in words:
        if word in compute:
            compute[word] += 1
        else:
            compute[word] = 1
    print(compute)
    v, w = min((-compute[word], word) for word in compute)
    print(v)
    return w


word2 = ['a', 'a', 'b', 'b', 'c', 'd', 'd', 'b']

print(majority(word2))
