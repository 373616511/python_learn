def closest_values(a):
    assert len(a) >= 2
    a.sort()
    print(a)
    m, n = min((a[i] - a[i - 1], i) for i in range(1, len(a)))
    print(m, n)
    return a[n - 1], a[n]


b = [1, 2333, 56, 67, 23, 29, 345, 346, 222]
print(b)
print(closest_values(b))
