a = [1, 2, 3]
b = a[:]
print(b)
print(a[1:])
print(a[:-1])
print(a[::-1])

M1 = [[0] * 10 for _ in range(10)]
# print(M1)

# for i in range(0, 10):
#     print(i)
#
# # 反转
# for i in range(9, -1, -1):
#     print(i)
