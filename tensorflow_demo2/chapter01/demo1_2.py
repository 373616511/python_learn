# 1.2　存取元素
import numpy as np
from numpy import random as nr

def test():
    np.random.seed(2018)
    nd = np.random.random([10])
    print(nd)
    # 获取指定位置的数据，获取第4个元素
    print(nd[3])
    # 截取一段数据
    print(nd[3:6])
    # 截取固定间隔数据
    print(nd[1:6:2])
    # 倒序取数
    print(nd[::-1])
    # 截取一个多维数组的一个区域内数据
    nd12 = np.arange(25).reshape([5, 5])
    print(nd12)
    print(nd12[1:3, 1:3])
    # 截取一个多维数组中，数值在一个值域之内的数据
    print(nd12[(nd12 > 3) & (nd12 < 10)])
    # 截取多维数组中，指定的行,如读取第2,3行
    print(nd12[[1, 2]])
    print(nd12[1:3, :])
    # 截取多维数组中，指定的列,如读取第2,3列
    print(nd12[:, 1:3])


def test2():
    a = np.arange(1, 25, dtype=float)
    print(a)
    c1 = nr.choice(a, size=(3, 4))  # size指定输出数组形状
    c2 = nr.choice(a, size=(3, 4), replace=False)  # replace缺省为True，即可重复抽取
    # 下式中参数p指定每个元素对应的抽取概率，默认为每个元素被抽取的概率相同
    c3 = nr.choice(a, size=(3, 4), p=a / np.sum(a))
    print("随机可重复抽取")
    print(c1)
    print("随机但不重复抽取")
    print(c2)
    print("随机但按制度概率抽取")
    print(c3)


if __name__ == '__main__':
    print('begin')
    # test()
    test2()