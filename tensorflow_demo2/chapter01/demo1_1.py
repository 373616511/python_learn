# 1.1　生成ndarray的几种方式
import numpy as np


# numpy提供了两种基本的对象：ndarray（N-dimensional array object）和
# ufunc（universal function object）。ndarray是存储单一数据类型的多维
# 数组，而ufunc则是能够对数组进行处理的函数。

# 1.1生成ndarray的几种方式
def test1():
    list1 = [3.14, 2.17, 0, 1, 2]

    nd1 = np.array(list1)
    print(nd1)
    print(type(nd1))


def test2():
    list2 = [[3.14, 2.17, 0, 1, 2], [1, 2, 3, 4, 5]]
    nd2 = np.array(list2)
    print(nd2)
    print(type(nd2))


def test3():
    nd = np.random.random([3, 3])
    print(nd)
    print(type(nd))


# random生成0到1之间的随机数；uniform
# 生成均匀分布随机数；randn生成标准正态的随机数；normal生成正态分
# 布；shuffle随机打乱顺序；seed设置随机数种子等
def test4():
    np.random.seed(123)
    nd5_1 = np.random.randn(2, 3)
    print(nd5_1)
    np.random.shuffle(nd5_1)
    print("随机打乱后数据")
    print(nd5_1)
    print(type(nd5_1))


def test5():
    # 生成全是0的3x3矩阵
    nd6 = np.zeros([3, 3])
    print(nd6)
    # 生成全是1的3x3矩阵
    nd7 = np.ones([3, 3])
    print(nd7)
    # 生成3阶的单位矩阵
    nd8 = np.eye(3)
    print(nd8)
    # 生成3阶对角矩阵
    print(np.diag([1, 2, 3]))


# 格式为：arange（[start，]stop[，step，]，dtype=None）
def test6():
    print(np.arange(10))
    print(np.arange(2, 10))
    print(np.arange(1, 4, 0.5))
    print(np.arange(9, -1, -1))


if __name__ == '__main__':
    print('begin')
    # 1.从已有数据中创建
    # test1()
    # test2()

    # 2.利用random模块生成ndarray
    # test3()
    # test4()

    # 3.创建特定形状的多维数组
    # test5()

    # 4.利用arange函数
    test6()
