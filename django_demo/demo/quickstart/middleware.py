from django.utils.deprecation import MiddlewareMixin

"""
所有的钩子函数都可以返回一个Response
一旦钩子函数返回了Response,整个请求的受理就结束了
"""


# 在process_request中处理请求，process_response处理响应。
#
#
# 在settings.py文件的中间件配置中我们刚刚的中间件。

class SimpleMiddleWare(MiddlewareMixin):

    def process_request(self, request):
        print('执行process_request,--路由--' + request.path)
        return None

    def process_response(self, request, response):
        print('执行process_response' + str(type(response)))
        return response
