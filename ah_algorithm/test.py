import numpy as np

a = [0] * 10
print(a[0] is a[1])
a[1] = 2
print(a)
print(a[0] is a[1])

b = np.zeros(10, dtype=int)
print(b[0] is b[1])
b[1] = 2
print(b)

c = np.full(10, None)
print(c)
