# 第1节 不撞南墙不回头-第三章第1节坑爹的奥数
n = 9
# 盒子
a = [0] * (n + 1)
# 标记
book = [0] * (n + 1)

t = 0


# print(a)
# step = 1
def dfs(step):
    global t
    # 满足条件输出排列
    if step == n + 1:
        if a[1] * 100 + a[2] * 10 + a[3] + a[4] * 100 + a[5] * 10 + a[6] == a[7] * 100 + a[8] * 10 + a[9]:
            print('%d%d%d+%d%d%d=%d%d%d' % (a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9]))
            t += 1
        return
    for i in range(1, n + 1):
        # print(i, end=' ')
        if book[i] == 0:  # book[i]=0表示i号扑克牌在手上
            # print(i)
            a[step] = i
            book[i] = i
            dfs(step + 1)  # 递归
            book[i] = 0  # 将牌拿回来


dfs(1)

print('共%d种方法' % (t / 2))  # 为什么要除以2？交换顺序的情况算一种所以除2
