# 排名算法
import random

a = [i for i in range(0, 50, 2)]
print(a)
num = random.randint(1, 48)
# 模拟最后一位
num = -1
print(num)
a.append(num)
a.sort()
print(a)
index = 0
# 查找坐标
for k, v in enumerate(a):
    if v == num:
        print('排序后的下标:%d' % k)
        index = k
        break
if index >= len(a) - 1:
    for k, v in enumerate(a):
        if k < 9:
            # 输出前9位
            print('(%d,%d)' % (k, v), end=' ')
        # 输出最后一个
    print('(%d,%d)' % (len(a), a[len(a) - 1]), end=' ')

if index >= len(a) - 2:
    for k, v in enumerate(a):
        if k < 8:
            # 输出前9位
            print('(%d,%d)' % (k, v), end=' ')
        # 输出最后一个
    print('(%d,%d)' % (index, a[index]), end=' ')
    print('(%d,%d)' % (len(a), a[len(a) - 1]), end=' ')

if 6 < index < len(a) - 2:
    for k, v in enumerate(a):
        if k < 6:
            # 输出前6位
            print('(%d,%d)' % (k, v), end=' ')
    # 输出中间值
    print('(%d,%d)' % (index - 1, a[index - 1]), end=' ')
    print('(%d,%d)' % (index, a[index]), end=' ')
    print('(%d,%d)' % (index + 1, a[index + 1]), end=' ')
    # 输出最后一个
    print('(%d,%d)' % (len(a), a[len(a) - 1]), end=' ')
if index <= 6:
    for k, v in enumerate(a):
        if k < 9:
            # 输出前9位
            print('(%d,%d)' % (k, v), end=' ')
    # 输出最后一个
    print('(%d,%d)' % (len(a), a[len(a) - 1]), end=' ')
