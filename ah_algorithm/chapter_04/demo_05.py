# 第5节 宝岛探险

class Note:

    def __init__(self):
        self.x = -1  # 横坐标
        self.y = -1  # 纵坐标


que = [Note() for i in range(0, 2501)]
a = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 1, 2, 1, 0, 0, 0, 0, 0, 2, 3],
     [0, 3, 0, 2, 0, 1, 2, 1, 0, 1, 2],
     [0, 4, 0, 1, 0, 1, 2, 3, 2, 0, 1],
     [0, 3, 2, 0, 0, 0, 1, 2, 4, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 1, 5, 3, 0],
     [0, 0, 1, 2, 1, 0, 1, 5, 4, 3, 0],
     [0, 0, 1, 2, 3, 1, 3, 6, 2, 1, 0],
     [0, 0, 0, 3, 4, 8, 9, 7, 5, 0, 0],
     [0, 0, 0, 0, 3, 7, 8, 6, 0, 1, 2],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]]

book = [[0 for i in range(0, 52)] for j in range(0, 52)]
# print(book)
# 下一步 右 下 坐 上
next_array = [[0, 1], [1, 0], [0, -1], [-1, 0]]

n = 10
m = 10
startx = 6
starty = 8
# 队列初始化
head = 1
tail = 1
que[tail].x = startx
que[tail].y = starty
tail += 1
book[startx][starty] = 1
sum = 1  # 总数

# 当队列不为空的时候循环
while head < tail:
    # 枚举四个方向
    for k, v in enumerate(range(0, 4)):
        # 计算下一步的坐标
        tx = que[head].x + next_array[k][0]
        ty = que[head].y + next_array[k][1]
        # 判断是否越界
        if tx < 1 or tx > n or ty < 1 or ty > m:
            continue

        # 判断是否陆地或者曾经是否走过
        if a[tx][ty] > 0 and book[tx][ty] == 0:
            sum += 1
            book[tx][ty] = 1
            # 将新的点加入队列
            que[tail].x = tx
            que[tail].y = ty
            tail += 1
    head += 1  # 当一个点扩容结束后，继续往下扩展

print("地图的大小%d" % sum)
# 遍历book
for k, v in enumerate(book):
    for k2, v2 in enumerate(v):
        if v2 == 1:
            print('*', end=' ')
        else:
            print('0', end=' ')
    print()
