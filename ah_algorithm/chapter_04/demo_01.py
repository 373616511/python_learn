# 第1节 不撞南墙不回头
n = 3
# 盒子
a = [0] * (n + 1)
# 标记
book = [0] * (n + 1)


def dfs(step):
    # 满足条件输出排列
    if step == n + 1:
        for k, v in enumerate(a):
            if k != 0:
                print(v, end=' ')
        print('', end='\n')
        return
    for i in range(1, n + 1):
        if book[i] == 0:
            a[step] = i
            book[i] = 1  # 将book[i]设为1，表示i号扑克牌已经不在手上
            dfs(step + 1)  # 递归
            book[i] = 0  # 将牌拿回来


if __name__ == '__main__':
    dfs(1)
