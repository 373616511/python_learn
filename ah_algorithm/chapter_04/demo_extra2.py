# 排名算法
import random
import copy

a = [i for i in range(0, 50, 2)]
print(a)
num = random.randint(1, 48)
# 计算新数据所在的下标
b = copy.deepcopy(a)
b.append(num)
b.sort()
index = -1
for k, v in enumerate(b):
    if v == num:
        print('在全部数据中的下标:%d' % k)
        index = k
        break
# 模拟最后一位
# num = -1
print(num)
a.sort()
book = a[0:8:1]
book.append(a[len(a) - 1])
book.append(num)
book.sort()
print(book)
for k, v in enumerate(book):
    if v == num:
        print('下标碰撞%d' % k)
        if k == 7:
            book[6] = b[index - 1]
            book[8] = b[index + 1]
        if k == 8:
            book[7] = b[index - 1]
        if k == 9:
            break
print(book)
