# 第1节 坑爹的奥数
import time


# ？？？+？？？=？？？，将数字1~9分别填入9个？中，每个数字只能使用一次使得等式成立。
# 例如：173+286=459 和 286+173=459 是同一个组合。
# 思路：
# 利用枚举：有序的尝试每一种可能。

def test1():
    i = 1
    while i <= 9:
        a = i * 10 + 3
        b = 30 + i
        print('a=%d' % a)
        print('b=%d' % b)
        if (a * 6528) == (b * 8256):
            print(i)
            break
        i += 1


def test2():
    array = [0 for i in range(1, 10)]
    a, b, c, d, e, f, g, h, i = array
    print(c)
    total = 0
    while a < 9:
        a = a + 1
        b = 0
        while b < 9:
            b = b + 1
            c = 0
            while c < 9:
                c = c + 1
                d = 0
                while d < 9:
                    d = d + 1
                    e = 0
                    while e < 9:
                        e = e + 1
                        f = 0
                        while f < 9:
                            f = f + 1
                            g = 0
                            while g < 9:
                                g = g + 1
                                h = 0
                                while h < 9:
                                    h = h + 1
                                    i = 0
                                    while i < 9:
                                        i += 1
                                        if a != b and a != c and a != d and a != e and a != f and a != g and a != h and a != i \
                                                and b != c and b != d and b != e and b != f and b != g and b != h and b != i \
                                                and c != d and c != e and c != f and c != g and c != h and c != i \
                                                and d != e and d != f and d != g and d != h and d != i \
                                                and e != f and e != g and e != h and e != i \
                                                and f != g and f != h and f != i \
                                                and g != h and g != i \
                                                and h != i:
                                            x = (a * 100 + b * 10 + c + d * 100 + e * 10 + f)
                                            y = (g * 100 + h * 10 + i)
                                            # print(x)
                                            # print(y)
                                            if x == y:
                                                total += 1
                                                print('%d%d%d+%d%d%d=%d%d%d' % (a, b, c, d, e, f, g, h, i))

    total = total / 2  # 交换顺序的情况算一种所以除2
    return total


if __name__ == '__main__':
    begin = int(time.time())
    print(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())))
    print('测试')
    # test1()
    t = test2()
    print('共%d种方法' % t)
    end = int(time.time())
    print(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())))
    print('耗时%d秒' % (end - begin))
