import copy

n = 10
a = [0 for i in range(n)]
b = [0] * n
a[0] = 1
b[0] = 1
print(a)
print(b)

a = [1, 2, 3, [4, 5, 6]]
b = a  # 浅拷贝: 引用对象
c = a.copy()  # 浅拷贝：深拷贝父对象（一级目录），子对象（二级目录）不拷贝，还是引用
d = copy.deepcopy(a)  # 深拷贝，包含对象里面的自对象的拷贝，所以原始对象的改变不会造成深拷贝里任何子元素的改变

a.append('AA')
a[3].append('BB')

print("a:", a)
print("b:", b)
print("c:", c)
print("d:", d)

dict1 = {'user': 'A',
         'num': [1, 2, 3]}

dict2 = dict1  # 浅拷贝: 引用对象
dict3 = dict1.copy()  # 浅拷贝：深拷贝父对象（一级目录），子对象（二级目录）不拷贝，还是引用
dict4 = copy.deepcopy(dict1)

dict1['user'] = 'BBB'
dict1['num'].remove(1)

# 输出结果
print("dict1: ", dict1)
print("dict2: ", dict2)
print("dict3: ", dict3)
print("dict4: ", dict4)
