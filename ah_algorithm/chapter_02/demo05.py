# 第 5 节 模拟链表
data = [0] * 101
right = [0] * 101
n = int(input('输入数字的个数：'))
i = 1
while i <= n:
    num = int(input('输入数字：'))
    # 读取数据
    data[i] = num
    i += 1

# 初始化数组right
i = 1
while i <= n:
    if i != n:
        right[i] = i + 1
    else:
        right[i] = 0
    i += 1
print(data)
print(right)
a = int(input('输入插入的数字：'))
# 直接在链表末尾添加一个数字
data[n + 1] = a
len = n + 1
# 从链表头部开始遍历
t = 1
while t != 0:
    if data[right[t]] > data[len]:
        # 新插入数的下一个结点标号等于当前结点的下一个结点编号
        right[len] = right[t]
        # 当前结点的下一个结点编号就是新插入数的编号
        right[t] = len
        break
    t = right[t]
# 输出链表中所有的数
t = 1
print(data)
print(right)
while t != 0:
    tt = t == len
    print(data[t], end=' ')
    t = right[t]
