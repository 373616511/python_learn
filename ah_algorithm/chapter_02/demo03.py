# 第 3 节 纸牌游戏——小猫钓鱼
class Queue:
    def __init__(self):
        self.data = [None] * 1000
        self.head = 0
        self.tail = 0
        self.name = None


class Stack:
    """top 用来存储栈顶，数组 data 用来存储栈中的元素，大小设置为 10。因为只有 9种不同的牌面"""

    def __init__(self):
        self.data = [None] * 10
        self.top = 0


def init(s: str, name: str) -> Queue:
    """初始化-发牌"""
    q = Queue()
    q.head = 1
    q.tail = 1
    q.name = name

    for v in list(s):
        # print(v)
        q.data[q.tail] = v
        q.tail += 1
    # print(q.data)
    # print(q.tail)
    return q


def play(s: Stack, q: Queue):
    """出牌函数"""
    # print(q.name + '出牌时，桌面上的牌:' + str(s.data))
    # print(q.name + '手中的牌:' + get_card(q))

    # 小哼先亮出一张牌
    t = q.data[q.head]
    # 枚举桌上的每一张牌与 t 进行比对
    flag = 0
    if s.top > 0:
        temp = s.top
        while temp > 0:
            if s.data[temp] == t:
                flag = 1
                break
            temp -= 1
    # 如果 flag 的值为 0 就表明小哼没能赢得桌上的牌，将打出的牌留在桌上。
    if flag == 0:
        q.head += 1
        s.top += 1
        s.data[s.top] = t
    # 如果 flag 的值为 1 就表明小哼可以赢得桌上的牌，需要将赢得的牌依次放入小哼的手中。
    if flag == 1:
        q.head += 1  # 小哼已经打出一张牌，所以要把打出的牌出队
        q.data[q.tail] = t  # 因为此轮可以赢牌，所以紧接着把刚才打出的牌又放到手中牌的末尾
        q.tail += 1
        # 把桌上可以赢得的牌（从当前桌面最顶部一张牌开始取，直至取到与打出的牌相同为止）依次放到手中牌的末尾
        while s.data[s.top] != t:
            q.data[q.tail] = s.data[s.top]
            s.data[s.top] = None
            q.tail += 1
            s.top -= 1
        # 取走最后一张相等的牌
        q.data[q.tail] = s.data[s.top]
        q.tail += 1
        s.data[s.top] = None
        s.top -= 1


def get_card(q: Queue) -> str:
    rs = ''
    temp = q.head
    while temp < q.tail:
        rs += q.data[temp]
        temp += 1
    return rs


def get_table_card(s: Stack) -> str:
    rs = ''
    temp = 0
    while temp < len(s.data):
        if s.data[temp]:
            rs += s.data[temp]
        temp += 1
    return rs


def print_result(q: Queue, s: Stack):
    if q.head != q.tail:
        print(q.name + '胜利')
        rs = get_card(q)
        print('手中的牌是：' + rs)
        print('桌面上的牌是：' + get_table_card(s))


def game(q1: Queue, q2: Queue, s: Stack):
    # print("游戏开始")
    while (q1.head < q1.tail) and (q2.head < q2.tail):
        # 当任意一人手中的牌全部出完时，游戏结束，对手获胜。
        if q2.head < q2.tail:
            play(s, q1)
        if q1.head < q1.tail:
            play(s, q2)
            # 输出比赛结果
    print_result(q1, s)
    print_result(q2, s)


if __name__ == '__main__':
    s1 = '241256'
    s2 = '313564'
    # s1 = input("输入小哼手中的牌：")
    # s2 = input("输入小哈手中的牌：")
    qu1 = init(s1, "小哼")
    qu2 = init(s2, '小哈')
    # print(get_card(qu1))
    # print(get_card(qu2))
    # 桌面上的牌
    stack = Stack()
    # 开始出牌
    game(qu1, qu2, stack)
