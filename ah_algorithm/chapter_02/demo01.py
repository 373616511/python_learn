import numpy as np

# 第 1 节 解密 QQ 号——队列
# 最后的输出就是 6 1 5 9 4 7 2 8 3
# head每个循环都加2，tail加1，相等的时候就结束循环了
if __name__ == '__main__':
    a = np.zeros(10, int)
    print(a)
    q = [0, 6, 3, 1, 7, 5, 8, 9, 2, 4]
    q = np.append(q, a)
    print(q)
    head = 1
    # tail 用来记录队列的队尾（即最后一位）的下一个位置
    tail = 10
    while head < tail:
        print(q[head], end=" ")
        head += 1
        q[tail] = q[head]
        tail += 1
        # 队首出队
        head += 1
    print(tail)
