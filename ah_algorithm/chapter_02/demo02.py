# 第 2 节 解密回文——栈
# xyzyx”是一个回文字符串，所谓回文字符串就是指正读反读均相同的字符序列，如“席主席”、“记书记”、“aha”和“ahaha”均是回
# 文，但“ahah”不是回文。通过栈这个数据结构我们将很容易判断一个字符串是否为回文。

import numpy as np


# :str 指定参数类型是str ；->bool 指定返回类型是bool
def is_palindrome(s: str) -> bool:
    # 定义长度是100的列表
    a = np.zeros(100, str)
    mid = len(s) // 2 - 1
    print(mid)
    top = 0
    # 将mid前的字符依次入栈
    i = 0
    while i <= mid:
        top += 1
        a[top] = s[i]
        i += 1
    # print(a)
    # 判断字符串的长度是奇数还是偶数，并找出需要进行字符匹配的起始下标
    if len(s) % 2 == 0:
        next_index = mid + 1
    else:
        next_index = mid + 2
    # 开始匹配
    n = len(s) - 1
    m = next_index
    while m <= n:
        if a[top] != s[m]:
            break
        m += 1
        top -= 1
    if top == 0:
        # print("yes")
        return True
    else:
        # print('no')
        return False


if __name__ == '__main__':
    # s = input("请输入字符串：")
    s = str(1234567654321)
    print(is_palindrome(s))
