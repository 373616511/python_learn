# 第 4 节 小哼买书
import numpy as np


# 时间复杂度 O(M+N)
# M:桶的个数
# N:待排序数的个数

def sort(a):
    b = np.zeros(1000, dtype=int)
    # 进行计数
    for v in a:
        temp = b[v]
        b[v] = temp + 1
    # print(b)
    print('排序结果:', end="\t")
    # 出现了几次都打印一次，以达到去重的目的
    for (i, v) in enumerate(b):
        if v > 0:
            print(i, end="\t")


if __name__ == '__main__':
    array = [20, 40, 32, 67, 40, 20, 89, 300, 400, 15]
    sort(array)
