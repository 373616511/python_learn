# 第 2 节 邻居好说话——冒泡排序

# 时间复杂度O(N2)
def bubble_sort(a):
    print(a)
    n = len(a)
    for i in range(n):  # 比较n-1趟
        for j in range(n - i - 1):
            # 从第一个数开始和后面的数比较，比较次数：n-i-1
            if a[j] < a[j + 1]:
                t = a[j + 1]
                a[j + 1] = a[j]
                a[j] = t


if __name__ == '__main__':
    a = [23, 456, 78, 8, 9, 23, 561, 323, 11, 8]
    bubble_sort(a)
    print("结果：")
    print(a)
