# 第 1 节 最快最简单的排序——桶排序
import numpy as np


# 时间复杂度 O(M+N)
# M:桶的个数
# N:待排序数的个数

def sort(a):
    b = np.zeros(11, dtype=int)
    # 进行计数
    for v in a:
        temp = b[v]
        b[v] = temp + 1
    print(b)
    print('排序结果:', end="\t")
    # 出现了几次就打印几次
    for (i, v) in enumerate(b):
        for j in range(1, v + 1):
            print(i, end="\t")


if __name__ == '__main__':
    array = [5, 2, 5, 3, 8]
    sort(array)
