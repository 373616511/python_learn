# 邻接表替换之前的邻接矩阵
u = [0 for i in range(0, 7)]
v = [0 for i in range(0, 7)]
w = [0 for i in range(0, 7)]
print(u)
# first 和 next数组的大小要根据实际情况来设置，要比n的最大值大1
first = [0 for i in range(0, 6)]
next_ = [0 for i in range(0, 6)]
n = 4  # 顶点个数
m = 5  # 边的个数
for i in range(1, n + 1):
    first[i] = -1
print(first)

a = [[0, 0, 0],
     [1, 4, 9],
     [2, 4, 6],
     [1, 2, 5],
     [4, 3, 8],
     [1, 3, 7]
     ]
for i in range(1, m + 1):
    # 读入每一条表
    u[i] = a[i][0]
    v[i] = a[i][1]
    w[i] = a[i][2]
    # 下面两句最关键
    next_[i] = first[u[i]]  # 存储编号为i的下一条边的编号
    first[u[i]] = i  # first：边第一个顶点的编号
print(first)
print(next_)

print('遍历1号顶点的每一条边:')
k = first[1]
while k != -1:
    print('%d,%d,%d' % (u[k], v[k], w[k]))
    k = next_[k]

print('遍历每个顶点的边')
for i in range(1, n + 1):
    k = first[i]
    while k != -1:
        print('%d,%d,%d' % (u[k], v[k], w[k]))
        k = next_[k]
