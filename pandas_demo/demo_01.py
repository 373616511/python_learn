# 推导式
def my_func(x):
    return x ** 2


given_list = [0, 1, 2, 3, 4]

list_ = list(my_func(i) for i in given_list)
print(list_)

# 字典推导式
dict_ = {i: my_func(i) for i in given_list}
print(dict_)
