# 入口函数
from work_zykj.oracle.oracle_breeder import *
from log_util import *


def do_exec():
    # 查询全部农场
    farm_org_list = find_farm_org()
    farm_org_list = json.loads(farm_org_list)
    print(type(farm_org_list))
    log.info('全部农场：' + str(len(farm_org_list)))
    log.info(farm_org_list)

    # 遍历农场查询每个农场的栋批
    for info in farm_org_list:
        rs = find_farm_org_location(info['ORG_CODE'], info['FARM_ORG'])
        log.info(rs)
        rs = json.loads(rs)
        # 遍历栋批查询每个栋批的库存
        for farm in rs:
            stock = find_stock(farm['FARM_ORG'], farm['ORG_CODE'])
            stock = json.loads(stock)
            for s in stock:
                if s['BAL_QTY'] is not None and s['BAL_QTY'] < 0:
                    log_error.info(
                        '数量小于0：' + str(s['BAL_QTY']) + ' ' + s['FARM_ORG'] + " " + s['ORG_CODE'] + ' ' + s[
                            'PRODUCT_SPEC'])
                    # 往回查询单据，查找使单据数量归0的单据


if __name__ == '__main__':
    log = Logger('C:/Users/yangjianzhang/Desktop/farm.log', level='info', filemode='W')
    log.info('start')
    log_error = Logger('C:/Users/yangjianzhang/Desktop/farm-error.log', level='info', filemode='W')
    try:
        do_exec()
    finally:
        o = Oracle_Util()
        o.disconnect()
