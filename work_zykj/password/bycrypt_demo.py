import bcrypt


# 加密密码
def password_hash(passwd):
    return bcrypt.hashpw(passwd, bcrypt.gensalt())


# 验证密码
def password_verify(passwd, hashed):
    return bcrypt.checkpw(passwd, hashed)


if __name__ == '__main__':
    password = "831223".encode('utf-8')
    hashed = '$2a$10$gioPWDbm9.BslafKs5NZceOUmzmxOfX6NbGI5opn19vi.mpOBbNMq'.encode('utf-8')
    print(password_hash(password))
    print(password_verify(password,hashed))