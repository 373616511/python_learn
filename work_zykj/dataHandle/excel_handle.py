import datetime
import json

import pandas as pd

import sql_util

if __name__ == '__main__':
    file_path = 'C:/Users/yangjianzhang/Desktop/2.xlsx'
    # 使用read_excel函数读取Excel文件并将其存储为DataFrame对象
    df = pd.read_excel(file_path)
    # 打印读取到的数据
    # print(df)
    print(type(df))
    org_code_list = []
    order_id_list = []
    flag_list = []
    for index, row in df.iterrows():
        # print(row['push_content'])
        push_content = row['push_content']
        push_content = json.loads(push_content)
        order_id = push_content['orderId']
        org_code = push_content['orgCode']
        # print('%s-%s' % (org_code, order_id))
        sql = "select * from zcn_scan_code t where t.order_id = '{}'".format(order_id)
        flag = '否'
        try:
            rs = sql_util.select_by_org(org_code, sql)
            rs = json.loads(rs)
            if len(rs) > 0:
                flag = '是'
        except Exception as e:
            print(e)
            flag = '异常' + str(e)
        org_code_list.append(org_code)
        order_id_list.append(order_id)
        flag_list.append(flag)
        print('%s-%s,数据是否存在:%s' % (org_code, order_id, flag))

    # 导出excel
    excel_data = {}
    excel_data['分公司'] = org_code_list
    excel_data['订单号'] = order_id_list
    excel_data['是否成功'] = flag_list

    df = pd.DataFrame(excel_data)
    path = "C:/Users/yangjianzhang/Desktop/"
    df.to_excel(path + 'test_rs_{}.xlsx'.format(datetime.datetime.now().strftime('%Y%m%d%H%M%S')), index=False)
