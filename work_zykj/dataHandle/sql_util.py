# sql工具类-猪系统
import json

from work_zykj.mysql import mysql_util
from work_zykj.oracle.oracle_util import oracle_util_class


def select_by_org(org_code, sql):
    # 查询数据源
    # 查询数据
    sql_mysql = "SELECT * FROM ms_data_source_info t where t.org_code = '{}' limit 1".format(org_code)
    rs = mysql_util.select_by_sql(sql_mysql)
    rs = json.loads(rs)
    if len(rs) < 1:
        print(sql_mysql)
        raise Exception('数据源不存在')
    schema = ''
    tag = rs[0]['tag']
    if 'CPGCNXL' in tag:
        schema = 'CPGCNXL'
    elif 'CTCNZQF' in tag:
        schema = 'CTCNZQF'
    elif 'NMDCFARM' in tag:
        schema = 'NMDCFARM'
    elif 'CRDCFARM' in tag:
        schema = 'CRDCFARM'
    print('%s数据源:%s' % (org_code, schema))
    oracle_util = oracle_util_class()
    rs = oracle_util.select(sql, schema)
    # print(type(rs))
    return rs


if __name__ == '__main__':
    org_code = '16162211'
    sql = "select 1 from dual"
    sql = "select * from zcn_scan_code t where t.order_id = '241104064600210'"
    rs = select_by_org(org_code, sql)
    print(rs)
