from openpyxl import load_workbook

file_name = '猪博士接口统计'


def write_txt(sql: str):
    # sql脚本
    with open('C:/Users/yangjianzhang/Desktop/' + file_name + '.txt', "a", encoding='UTF-8') as f:
        f.write(sql)  # 自带文件关闭功能，不需要再写f.close()
        f.write("\n")


def read_excel():
    path = 'C:/Users/yangjianzhang/Desktop/' + file_name + '.xlsx'
    # 打开文件
    wb = load_workbook(path)
    # 获取所有sheet的名字
    print(wb.sheetnames)
    s = wb['工作表1']
    print(s)
    cells = s['B2:B27']

    rs = '('
    # 循环获取行
    for cell_line in cells:
        # 循环获取单个单元格
        for cell in cell_line:
            rs = rs + "'" + cell.value + "',"
            # print(cell.value)

    print(rs)


if __name__ == '__main__':
    read_excel()
