import jaydebeapi

# https://pypi.org/project/JayDeBeApi/
if __name__ == '__main__':
    print('开始测试')

    username = "MBI"
    password = "MBI"

    # db_conn = cx_Oracle.connect('MBI/MBI@scan-lydb.cpgroup.cn:1521/CTCNELYS')
    # url写法二
    url = "jdbc:oracle:thin:@//scan-lydb.cpgroup.cn:1521/CTCNELYS"
    driver = 'oracle.jdbc.OracleDriver'
    jarFile = './jars/ojdbc8-12.2.0.1.jar'
    conn = jaydebeapi.connect(driver, url, [username, password], jarFile)
    # 其中mysql的用户名和密码都是hive,最后一个参数是驱动的jar包

    curs = conn.cursor()

    sql = "select count(company_code) from MV_IFPIN_SWINE"
    curs.execute(sql)
    rs = curs.fetchall()
    print(rs)
    curs.close()

    conn.close()
