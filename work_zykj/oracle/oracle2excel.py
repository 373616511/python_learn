import os

import cx_Oracle
import pandas as pd
import time


def getTime():
    times = time.time()
    local_time = time.localtime(times)
    return time.strftime("%Y%m%d%H%M%S", local_time)


def bg_header(x):
    return "background-color: red"


if __name__ == '__main__':
    user = "pigpos"
    passwd = "pigposdev"
    listener = 'TESTZQF.cpchina.cn/TESTZQF'
    # 注意：解决中文乱码；
    os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'
    conn = cx_Oracle.connect(user, passwd, listener)

    path = "C:/Users/yangjianzhang/Desktop/test" + getTime() + ".xls"
    sql = "select * from fr_trn_pr_header t where t.org_code = '053622118' and t.pr_document_no in ( '1118160000100','1118160000292')"
    df = pd.read_sql(sql, con=conn)
    print(type(df))

    yellow_css = 'background-color: yellow'

    df.style.applymap_index(bg_header, axis=1).to_excel('output.xlsx', index=False)

    # 转换为excel进行保存
    # df.to_excel(path)
    # 背景颜色

    conn.close()
