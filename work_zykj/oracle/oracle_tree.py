# 查找表依赖路径
import json


def find_table(json_data, path, table_name):
    # print(json_data)
    path.append(json_data[0]['NAME'])
    for k in json_data:
        ref_table = k['REFERENCED_NAME']
        # 找到了对应的table_name
        if ref_table == table_name:
            path.append(ref_table)
            # print(path)
            print(' -> '.join([str(i) for i in path]))
            path.pop()
        if 'dependencies' in k:
            ref_dep = k['dependencies']
            # print(type(ref_dep))
            if type(ref_dep) == list:
                # print(ref_table)
                find_table(ref_dep, path, table_name)

    path.pop()


if __name__ == '__main__':
    print('开始查询')
    t = 'FR_SW_JOB_COST'
    table_name = 'FR_SW_COS_FINISHER_HEADER'
    jsonFilePath = "C:/Users/yangjianzhang/Desktop/" + t + ".json"
    path = []
    try:
        with open(jsonFilePath) as f:
            j = json.load(f)
        # print(j)
        print(type(j) == list)
        find_table(j, path, table_name)

    finally:
        f.close()
