import json


# 查找存储里面的表的 DQL DML
def find_type(s: str):
    if s.find('UDATE') > -1:
        return 'UPDATE'
    elif s.find('SELECT') > -1:
        return 'SELECT'
    elif s.find('DELETE') > -1:
        return 'DELETE'
    elif s.find('INSERT') > -1:
        return 'INSERT'
    else:
        return 'OTHER'


if __name__ == '__main__':
    print('开始查询')
    t = 'FR_SW_JOB_COST'
    jsonFilePath = "C:/Users/yangjianzhang/Desktop/" + t + ".json"
    file = open("C:/Users/yangjianzhang/Desktop/" + t + ".txt", "r")
    d = {}
    try:
        with open(jsonFilePath) as f:
            j = json.load(f)
        print(j)
        str = file.read();
        for table in j:
            field = table['REFERENCED_NAME']
            count = str.count(field)
            print(field, count)
            if count == 0:
                continue
            ind = 1
            for i in range(0, count):
                index = str.index(field, ind)
                s = str[index - 30: index]
                # print(s)
                if field in d:
                    val = d[field]
                    ft = find_type(s);
                    if val.find(ft) < 0:
                        d[field] = val + ',' + ft
                else:
                    d[field] = find_type(s)
                ind = index + 1


    finally:
        file.close()

    print(d)
