import jaydebeapi

# https://pypi.org/project/JayDeBeApi/
if __name__ == '__main__':
    print('开始测试')

    username = "thai_bi"
    password = "tb#24!CuV"
    # url写法一
    url = "jdbc:oracle:thin:@(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = TESTZQF.cpchina.cn)(PORT = 1521))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME = TESTZQF)))"
    # url写法二
    url = "jdbc:oracle:thin:@//10.0.10.77:1521/bwdb"
    driver = 'oracle.jdbc.OracleDriver'
    jarFile = '../jars/ojdbc8-12.2.0.1.jar'
    conn = jaydebeapi.connect(driver, url, [username, password], jarFile)
    # 其中mysql的用户名和密码都是hive,最后一个参数是驱动的jar包

    curs = conn.cursor()

    sql = "select * from ZEGG_BD_INPUT_EGGWGH where rownum < 10"
    curs.execute(sql)
    rs = curs.fetchall()
    print(rs)
    curs.close()

    conn.close()
