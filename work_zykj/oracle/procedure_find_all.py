import datetime
import json
import os

import cx_Oracle

"""在整个数据库的存储中匹配字符串"""

user = "pigpos"
passwd = "P1g#2023pos"

# user = "DW"
# passwd = 'dw@cpgroup'
# listener = 'CPGCNXL.cpchina.cn/CPGCNXL'
# listener = '10.240.24.135/NMDCFARM'
listener = 'CTCNZQF.cpchina.cn/CTCNZQF'
# listener = 'dw.CPCHINA.CN/dw'
conn = cx_Oracle.connect(user, passwd, listener)
cursor = conn.cursor()
table_list = []


def select(sql) -> json:
    rows = []
    cursor.execute(sql)
    result = cursor.fetchall()
    col_name = cursor.description
    for row in result:
        d = {}
        for col in range(len(col_name)):
            key = col_name[col][0]
            value = row[col]
            if isinstance(value, datetime.datetime):
                value = value.strftime('%Y-%m-%d %H:%M:%S')
            d[key] = value
        rows.append(d)
    js = json.dumps(rows, sort_keys=True, ensure_ascii=False, separators=(',', ':'))
    return js


# 关闭连接，释放资源
def disconnect():
    cursor.close()
    conn.close()
    print("关闭数据库连接")


def write2txt(cur_procedure, rs):
    # print('写文件')
    path = "C:/Users/yangjianzhang/Desktop/procedure"
    if not os.path.exists(path):
        os.mkdir(path)
    file = path + "/" + cur_procedure + ".txt"
    with open(file, mode='w', encoding='utf-8') as f:
        j = json.loads(rs)
        for text in j:
            # print(text['TEXT'])
            f.write(text['TEXT'])


def find(key, cur_procedure, path):
    # 查询当前存储是否包括
    path = path + "->" + cur_procedure
    # print(path)
    sql = " SELECT text FROM user_source WHERE NAME = '" + cur_procedure + "' ORDER BY line"
    # 查询依赖存储是否包括
    rs = select(sql)
    # 将存储写到文件
    write2txt(cur_procedure, rs)
    # print(type(rs))
    a = 0
    j = json.loads(rs)
    content = ''
    # 整理存储的字符
    for text in j:
        content = content + text['TEXT']
    content = content.replace('\n', '')
    content = content.replace('\t', '')
    content = content.replace(' ', '')
    if key.upper() in content.upper():
        a = 1
        pass
    if a == 1:
        print('存储:%s,是否包含:%d,路径:%s' % (cur_procedure, a, path))
        if cur_procedure not in table_list:
            table_list.append(cur_procedure)


# 消除空白字符串后再去查询更精确
def findAllProcedure():
    # sql = "SELECT OBJECT_NAME, OBJECT_TYPE FROM USER_OBJECTS WHERE OBJECT_TYPE IN ('PROCEDURE', 'FUNCTION', 'PACKAGE', 'PACKAGE BODY') order by object_name"
    sql = "SELECT OBJECT_NAME, OBJECT_TYPE FROM USER_OBJECTS WHERE OBJECT_TYPE IN ('PROCEDURE') order by object_name"
    rs = select(sql)
    return json.loads(rs)


if __name__ == '__main__':
    # 查询报错中文描述
    """ SELECT  *   FROM MAS_ERROR_MESSAGE where  error_id = 'FRCHK074'"""
    print('开始查询')

    key = "FR_SW_COS_FINISHER_RA505"
    print('key:%s' % key)
    path = ''
    array = findAllProcedure()
    for o in array:
        cur_procedure = o['OBJECT_NAME']
        find(key, cur_procedure, path)
    print(key + ' 结果：')
    for t in table_list:
        print(t)
