import pandas as pd
import mysql_util as sql_util
import json
import math

"""更新pc_icon表的埋点配置"""


def update_icon(name, event_tracking):
    sql = "update pc_icon set event_tracking = '{}' where name = '{}'".format(event_tracking, name)
    print(sql)
    sql_util.execute(sql)


if __name__ == '__main__':
    file_path = 'C:/Users/yangjianzhang/Desktop/pc_icon.xlsx'
    # 使用read_excel函数读取Excel文件并将其存储为DataFrame对象
    df = pd.read_excel(file_path, sheet_name='pc_icon的埋点')

    # 打印读取到的数据
    # print(df)
    print(type(df))
    flags = []
    for index, row in df.iterrows():
        name = row['name']
        event_tracking = row['event_tracking']
        if not pd.isna(event_tracking) and event_tracking != '无':
            print('-- %s - %s' % (name, event_tracking))
            update_icon(name, event_tracking)
