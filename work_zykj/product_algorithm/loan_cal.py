import pandas as pd

month_list = []
monthly_principal_list = []
monthly_interest_list = []
monthly_payment_list = []


# 等额本金
def calculate_equal_principal(loan_amount, annual_interest_rate, loan_term):
    monthly_interest_rate = annual_interest_rate / 100
    total_payments = loan_term

    monthly_principal = loan_amount / total_payments
    total_interest = 0

    print("每月还款详情：")
    for month in range(1, total_payments + 1):
        monthly_interest = (loan_amount - (month - 1) * monthly_principal) * monthly_interest_rate
        monthly_payment = monthly_principal + monthly_interest
        total_interest += monthly_interest

        print(f"第{month}月：本金{monthly_principal:.2f} + 利息{monthly_interest:.2f} = {monthly_payment:.2f}")
        month_list.append(month)
        # print(type("{:.2f}".format(monthly_principal)))
        monthly_principal_list.append(float("{:.2f}".format(monthly_principal)))
        monthly_interest_list.append(float("{:.2f}".format(monthly_interest)))
        monthly_payment_list.append(float("{:.2f}".format(monthly_payment)))

    print(f"\n总还款额：{loan_amount + total_interest:.2f}")
    print(f"支付利息总额：{total_interest:.2f}")


# 等额本息
def calculate_equal_installment_principal(loan_amount, annual_interest_rate, loan_term):
    monthly_interest_rate = annual_interest_rate / 100
    total_payments = loan_term

    monthly_payment = (loan_amount * monthly_interest_rate * (1 + monthly_interest_rate) ** total_payments) / \
                      ((1 + monthly_interest_rate) ** total_payments - 1)

    total_payment = monthly_payment * total_payments
    total_interest = total_payment - loan_amount

    print(f"每月还款额：{monthly_payment:.2f}")
    print(f"总还款额：{total_payment:.2f}")
    print(f"支付利息总额：{total_interest:.2f}")


if __name__ == '__main__':
    # 输入贷款信息
    # loan_amount = float(input("请输入贷款金额："))
    loan_amount = 71900
    # annual_interest_rate = float(input("请输入月利率（%）："))
    annual_interest_rate = 0.416
    # loan_term = int(input("请输入贷款期限（月）："))
    loan_term = 60

    # 计算等额本金
    calculate_equal_principal(loan_amount, annual_interest_rate, loan_term)

    # 导出excel
    # 创建一个简单的DataFrame
    data = {'月': month_list,
            '本金': monthly_principal_list,
            '利息': monthly_interest_list,
            '还款': monthly_payment_list,
            }
    df = pd.DataFrame(data)

    # 导出到Excel文件
    df.to_excel('C:/Users/yangjianzhang/Desktop/等额本金还款计划.xlsx', index=False)
