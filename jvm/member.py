from attribute import *


class MemberEngine:
    @staticmethod
    def read_members(class_reader, constant_pool):
        m_len = class_reader.read_u2()
        members = []
        for i in range(0, m_len):
            members.append(MemberEngine.make_member(class_reader, constant_pool))
        return members

    @staticmethod
    def make_member(class_reader, constant_pool):
        return MemberInfo(class_reader, constant_pool)


class MemberInfo:
    def __init__(self, class_reader, constant_pool):
        self.reader = class_reader
        self.constant_pool = constant_pool
        self.accessFlags = class_reader.read_u2()
        # 名称(方法名或者变量名) 在常量池的下标
        self.nameIndex = class_reader.read_u2()
        # 方法或变量 描述符 在常量池的下标
        self.descriptorIndex = class_reader.read_u2()
        # 读取 方法的属性表
        self.attrs = AttributeEngine.read_attributes(class_reader, constant_pool)
