import struct


# 解析 class字节码工具
# Java虚拟机规范定义了u1、u2和u4三种数据类型来表示1、2和4字节无符号整数
class ClassReader:
    def __init__(self, byte_codes):
        self.byte_codes = byte_codes

    # 读取 u1 (1字节)
    def read_u1(self):
        # 取出第一个字节
        v = self.byte_codes[0]
        # 截取掉取出的字节
        self.byte_codes = self.byte_codes[1:]
        return v

    # 读取u2 （2字节）
    def read_u2(self):
        # 取出两个字节
        v = self.byte_codes[0:2]
        # > 大端字节序
        a = struct.unpack('>H', v)[0]
        self.byte_codes = self.byte_codes[2:]
        return a

    # 读取u4 （4字节）
    def read_u4(self):
        # 取出4个字节
        v = self.byte_codes[0:4]
        a = struct.unpack('>I', v)[0]
        self.byte_codes = self.byte_codes[4:]
        return a

    # 读取u8 （8字节）
    def read_u8(self):
        # 取出8个字节
        v = self.byte_codes[0:8]
        a = struct.unpack('>q', v)[0]
        self.byte_codes = self.byte_codes[8:]
        return a

    # 读取u2数组
    def read_u2s(self):
        u2_array = []
        n = self.read_u2()
        for i in range(0, n):
            u2_array.append(self.read_u2())
        return u2_array

    # 读取 指定数量的字节
    def read_bytes(self, n):
        v = self.byte_codes[0:n]
        self.byte_codes = self.byte_codes[n:]
        return v

    def get_byte_codes(self):
        return str(list(self.byte_codes))
