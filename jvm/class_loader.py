from common import *


# 类加载 接口
class ClassLoader:
    # 加载class得到字节流
    def load_class(self, class_name):
        pass


# 目录加载器 （用于加载classpath用户的类）
class DirClassLoader(ClassLoader):
    def __init__(self, path):
        self.path = path

    def load_class(self, class_name):
        # 获取完整class文件路径
        full_path = self.path + class_name + ".class"
        # 读取文件字节流
        return read_file_of_bytes(full_path)

    def to_string(self):
        return self.path


if __name__ == '__main__':
    d = DirClassLoader('C:/Users/yangjianzhang/Desktop/')
    byte_codes = d.load_class('Test')
    print("加载到Test主类的字节码: " + str(list(byte_codes)))
    a = list(hex(i) for i in list(byte_codes))
    print(a)
    # print(len(a)) #643
    # print(len(byte_codes)) #643
    print(byte_codes)
    v = byte_codes[0:11]
    print(v)
