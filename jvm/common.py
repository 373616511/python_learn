# 读取文件得到二进制流 bytes
def read_file_of_bytes(path):
    with open(path, mode='rb') as f:
        data = f.read()
    f.close()
    return data
