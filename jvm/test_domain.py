import sys

from jvm.class_loader import DirClassLoader
from jvm.class_reader_engine import ClassReaderEngine
'''解析class字节码'''
if __name__ == '__main__':
    print('大小端:', sys.byteorder)  # little 小端
    print('编码:', sys.getdefaultencoding())
    d = DirClassLoader('C:/Users/yangjianzhang/Desktop/')
    byte_codes = d.load_class('Test')
    cre = ClassReaderEngine(byte_codes)
    cre.reader()
    cre.to_string()
